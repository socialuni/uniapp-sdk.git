import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const uniStore = new Vuex.Store({})

export default uniStore
