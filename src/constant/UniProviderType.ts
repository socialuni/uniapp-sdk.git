export default class UniProviderType {
  static readonly wx: string = 'weixin'
  static readonly qq: string = 'qq'
}
