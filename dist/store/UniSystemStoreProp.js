"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UniSystemStoreProp = /** @class */ (function () {
    function UniSystemStoreProp() {
    }
    UniSystemStoreProp.systemInfo = 'systemInfo';
    UniSystemStoreProp.system = 'system';
    UniSystemStoreProp.platform = 'platform';
    UniSystemStoreProp.mp = 'mp';
    UniSystemStoreProp.isIos = 'isIos';
    UniSystemStoreProp.notIos = 'notIos';
    UniSystemStoreProp.isAndroid = 'isAndroid';
    UniSystemStoreProp.notAndroid = 'notAndroid';
    UniSystemStoreProp.isApp = 'isApp';
    UniSystemStoreProp.notApp = 'notApp';
    UniSystemStoreProp.isMp = 'isMp';
    UniSystemStoreProp.notMp = 'notMp';
    UniSystemStoreProp.isH5 = 'isH5';
    UniSystemStoreProp.notH5 = 'notH5';
    UniSystemStoreProp.isQQ = 'isQQ';
    UniSystemStoreProp.notQQ = 'notQQ';
    UniSystemStoreProp.isWX = 'isWX';
    UniSystemStoreProp.notWX = 'notWX';
    return UniSystemStoreProp;
}());
exports.default = UniSystemStoreProp;
//# sourceMappingURL=UniSystemStoreProp.js.map