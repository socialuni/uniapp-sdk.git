/// <reference types="@dcloudio/types" />
import { RegisterOptions, VuexModule } from 'vuex-class-modules';
import GetSystemInfoResult = UniApp.GetSystemInfoResult;
export default class UniSystemModule extends VuexModule {
    system: string;
    systemInfo: GetSystemInfoResult;
    platform: string;
    mpPlatform: string;
    isIos: boolean;
    notIos: boolean;
    isAndroid: boolean;
    notAndroid: boolean;
    isApp: boolean;
    notApp: boolean;
    isMp: boolean;
    notMp: boolean;
    isH5: boolean;
    notH5: boolean;
    isQQ: boolean;
    notQQ: boolean;
    isWX: boolean;
    notWX: boolean;
    constructor(options: RegisterOptions);
}
