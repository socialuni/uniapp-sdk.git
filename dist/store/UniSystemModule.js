"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var vuex_class_modules_1 = require("vuex-class-modules");
var UniSystemType_1 = tslib_1.__importDefault(require("../constant/UniSystemType"));
var UniPlatformType_1 = tslib_1.__importDefault(require("../constant/UniPlatformType"));
var UniMpPlatformType_1 = tslib_1.__importDefault(require("../constant/UniMpPlatformType"));
//用来存储当前用户的一些信息
var UniSystemModule = /** @class */ (function (_super) {
    tslib_1.__extends(UniSystemModule, _super);
    function UniSystemModule(options) {
        var _this = _super.call(this, options) || this;
        // 条件编译属性
        // ios android h5,默认安卓
        _this.system = '';
        _this.systemInfo = null;
        _this.platform = '';
        // 小程序类型
        _this.mpPlatform = '';
        _this.isIos = false;
        _this.notIos = false;
        _this.isAndroid = false;
        _this.notAndroid = false;
        //初始化以后第一步加载
        // 条件编译属性
        _this.isApp = false;
        _this.notApp = false;
        _this.isMp = false;
        _this.notMp = false;
        _this.isH5 = false;
        _this.notH5 = false;
        _this.isQQ = false;
        _this.notQQ = false;
        _this.isWX = false;
        _this.notWX = false;
        var systemInfo = uni.getSystemInfoSync();
        _this.systemInfo = systemInfo;
        var model = systemInfo.model;
        var platform = systemInfo.platform;
        //设置系统
        if ((platform && (platform === UniSystemType_1.default.ios)) || (model && (model.indexOf('iPhone') > -1 || model.indexOf('iPad') > -1))) {
            _this.isIos = true;
            _this.notIos = !_this.isIos;
            //必须有此判断，要不然会覆盖mp的值
            _this.system = UniSystemType_1.default.ios;
        }
        else {
            _this.isAndroid = true;
            _this.notAndroid = !_this.isAndroid;
            _this.system = UniSystemType_1.default.android;
        }
        //设置平台
        // #ifdef APP-PLUS
        _this.isApp = true;
        _this.notApp = !_this.isApp;
        _this.platform = UniPlatformType_1.default.app;
        // #endif
        // #ifdef MP
        _this.isMp = true;
        _this.notMp = !_this.notMp;
        _this.platform = UniPlatformType_1.default.mp;
        // #ifdef MP-WEIXIN
        _this.isWX = true;
        _this.notWX = !_this.isWX;
        _this.mpPlatform = UniMpPlatformType_1.default.wx;
        // #endif
        // #ifdef MP-QQ
        _this.isQQ = true;
        _this.notQQ = !_this.isQQ;
        _this.mpPlatform = UniMpPlatformType_1.default.qq;
        // #endif
        // #endif
        // #ifdef H5
        _this.isH5 = true;
        _this.notH5 = !_this.isH5;
        _this.platform = UniPlatformType_1.default.h5;
        return _this;
        // #endif
    }
    UniSystemModule = tslib_1.__decorate([
        vuex_class_modules_1.Module({ generateMutationSetters: true })
    ], UniSystemModule);
    return UniSystemModule;
}(vuex_class_modules_1.VuexModule));
exports.default = UniSystemModule;
//# sourceMappingURL=UniSystemModule.js.map