export default class UniSystemStoreProp {
    static readonly systemInfo = "systemInfo";
    static readonly system = "system";
    static readonly platform = "platform";
    static readonly mp = "mp";
    static readonly isIos = "isIos";
    static readonly notIos = "notIos";
    static readonly isAndroid = "isAndroid";
    static readonly notAndroid = "notAndroid";
    static readonly isApp = "isApp";
    static readonly notApp = "notApp";
    static readonly isMp = "isMp";
    static readonly notMp = "notMp";
    static readonly isH5 = "isH5";
    static readonly notH5 = "notH5";
    static readonly isQQ = "isQQ";
    static readonly notQQ = "notQQ";
    static readonly isWX = "isWX";
    static readonly notWX = "notWX";
}
