"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//小程序类型
var UniMpPlatformType = /** @class */ (function () {
    function UniMpPlatformType() {
    }
    //各平台的小程序
    UniMpPlatformType.qq = 'qq';
    UniMpPlatformType.wx = 'weixin';
    return UniMpPlatformType;
}());
exports.default = UniMpPlatformType;
//# sourceMappingURL=UniMpPlatformType.js.map