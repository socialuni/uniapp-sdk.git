"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UniProviderType = /** @class */ (function () {
    function UniProviderType() {
    }
    UniProviderType.wx = 'weixin';
    UniProviderType.qq = 'qq';
    return UniProviderType;
}());
exports.default = UniProviderType;
//# sourceMappingURL=UniProviderType.js.map