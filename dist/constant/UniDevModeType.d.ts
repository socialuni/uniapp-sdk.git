export default class UniDevModeType {
    static readonly dev: string;
    static readonly prod: string;
}
