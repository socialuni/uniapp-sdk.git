export default class UniSystemType {
    static readonly ios: string;
    static readonly android: string;
}
