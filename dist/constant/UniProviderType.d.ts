export default class UniProviderType {
    static readonly wx: string;
    static readonly qq: string;
}
