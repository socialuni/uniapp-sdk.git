"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UniPlatformType = /** @class */ (function () {
    function UniPlatformType() {
    }
    //app、mp、h5
    UniPlatformType.mp = 'mp';
    UniPlatformType.app = 'app';
    UniPlatformType.h5 = 'h5';
    return UniPlatformType;
}());
exports.default = UniPlatformType;
//# sourceMappingURL=UniPlatformType.js.map