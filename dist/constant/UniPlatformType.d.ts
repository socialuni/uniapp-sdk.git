export default class UniPlatformType {
    static readonly mp: string;
    static readonly app: string;
    static readonly h5: string;
}
