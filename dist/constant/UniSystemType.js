"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UniSystemType = /** @class */ (function () {
    function UniSystemType() {
    }
    UniSystemType.ios = 'ios';
    UniSystemType.android = 'android';
    return UniSystemType;
}());
exports.default = UniSystemType;
//# sourceMappingURL=UniSystemType.js.map