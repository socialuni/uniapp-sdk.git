"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UniDevModeType = /** @class */ (function () {
    function UniDevModeType() {
    }
    //区分开发和生产模式，使用不同userkey否则tokek，user信息会冲突
    UniDevModeType.dev = 'development';
    UniDevModeType.prod = 'production';
    return UniDevModeType;
}());
exports.default = UniDevModeType;
//# sourceMappingURL=UniDevModeType.js.map