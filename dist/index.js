"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uniSystemStore = exports.uniSystemModule = void 0;
var tslib_1 = require("tslib");
var store_1 = tslib_1.__importDefault(require("./store"));
var vuex_class_1 = require("vuex-class");
var UniSystemModule_1 = tslib_1.__importDefault(require("./store/UniSystemModule"));
var UniStoreModuleName_1 = tslib_1.__importDefault(require("./store/UniStoreModuleName"));
var unisdk = {
    install: function (Vue, options) {
        var store;
        if (options && options.store) {
            store = options.store;
        }
        else {
            //如果不传入store
            store = store_1.default;
        }
        Vue.prototype.$store = store;
        exports.uniSystemModule = new UniSystemModule_1.default({ store: store, name: UniStoreModuleName_1.default.system });
    }
};
exports.default = unisdk;
exports.uniSystemStore = vuex_class_1.namespace(UniStoreModuleName_1.default.system);
//# sourceMappingURL=index.js.map