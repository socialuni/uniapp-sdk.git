/**
 * @Author qingchi
 * @Date 2021-06-21 23:38
 * @Version 1.0
 */
import { PluginObject } from "vue";
import UniSystemModule from "./store/UniSystemModule";
export declare let uniSystemModule: UniSystemModule;
declare const unisdk: PluginObject<null>;
export default unisdk;
export declare const uniSystemStore: import("vuex-class/lib/bindings").BindingHelpers;
