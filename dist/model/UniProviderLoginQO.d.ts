export default class UniProviderLoginQO {
    provider: string;
    platform: string;
    code: string;
    openId: string;
    unionId: string;
    encryptedData: string;
    iv: string;
    nickName: string;
    avatarUrl: string;
    gender: number;
    birthday: string;
    country: string;
    province: string;
    city: string;
    constructor();
}
