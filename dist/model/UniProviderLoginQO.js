"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("../index");
var UniProviderLoginQO = /** @class */ (function () {
    function UniProviderLoginQO() {
        this.provider = '';
        this.platform = '';
        //小程序三方登录使用
        this.code = '';
        this.openId = '';
        this.unionId = '';
        //微信mp获取unionId使用
        this.encryptedData = '';
        this.iv = '';
        //微信有活在梦里
        this.nickName = '';
        //微信有url
        this.avatarUrl = '';
        //微信有1,2
        this.gender = 0;
        //qq会给生日
        this.birthday = '';
        //微信有China
        this.country = '';
        //微信有Beijing
        this.province = '';
        //微信有Chaoyang
        this.city = '';
        this.platform = index_1.uniSystemModule.platform;
    }
    return UniProviderLoginQO;
}());
exports.default = UniProviderLoginQO;
//# sourceMappingURL=UniProviderLoginQO.js.map