export default class UniUserInfoRO {
    openId: string;
    unionId: string;
    nickName: string;
    avatarUrl: string;
    gender: number;
    gender_type: number;
    year: string;
    country: string;
    province: string;
    city: string;
}
