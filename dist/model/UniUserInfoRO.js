"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//三方平台登录返回的用户信息结构
var UniUserInfoRO = /** @class */ (function () {
    function UniUserInfoRO() {
        this.openId = '';
        this.unionId = '';
        //微信有活在梦里
        this.nickName = '';
        //微信有url
        this.avatarUrl = '';
        //微信有1,2
        this.gender = null;
        //app下qq为gender_type
        this.gender_type = null;
        //app下qq为birthday为year
        this.year = null;
        //微信有China
        this.country = '';
        //微信有Beijing
        this.province = '';
        //微信有Chaoyang
        this.city = '';
    }
    return UniUserInfoRO;
}());
exports.default = UniUserInfoRO;
//# sourceMappingURL=UniUserInfoRO.js.map