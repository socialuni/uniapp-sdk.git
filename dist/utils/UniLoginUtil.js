"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var UniPlatformType_1 = tslib_1.__importDefault(require("../constant/UniPlatformType"));
var JsonUtil_1 = tslib_1.__importDefault(require("./JsonUtil"));
var index_1 = require("../index");
var UniLoginUtil = /** @class */ (function () {
    function UniLoginUtil() {
    }
    UniLoginUtil.getLoginCode = function (provider) {
        return UniLoginUtil.login(provider).then(function (loginRes) {
            if (UniPlatformType_1.default.mp === index_1.uniSystemModule.platform) {
                //小程序平台获取code
                return loginRes.code;
            }
            else {
                //app平台使用access_token 作为code
                return JsonUtil_1.default.deepClone(loginRes.authResult).access_token;
            }
        });
    };
    UniLoginUtil.login = function (provider) {
        return new Promise(function (resolve, reject) {
            uni.login({
                provider: provider,
                success: function (loginRes) {
                    resolve(loginRes);
                },
                fail: function (e) {
                    reject(e);
                }
            });
        });
    };
    return UniLoginUtil;
}());
exports.default = UniLoginUtil;
//# sourceMappingURL=UniLoginUtil.js.map