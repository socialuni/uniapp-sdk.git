"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var UniProviderLoginQO_1 = tslib_1.__importDefault(require("../model/UniProviderLoginQO"));
var UniLoginUtil_1 = tslib_1.__importDefault(require("../utils/UniLoginUtil"));
var UniProviderType_1 = tslib_1.__importDefault(require("../constant/UniProviderType"));
var index_1 = require("../index");
var UniUserUtil = /** @class */ (function () {
    function UniUserUtil() {
    }
    UniUserUtil.getUniProviderLoginQO = function (provider) {
        return Promise.all([UniLoginUtil_1.default.getLoginCode(provider), UniUserUtil.getUserInfo(provider)]).then(function (res) {
            var userInfo = new UniProviderLoginQO_1.default();
            userInfo.platform = index_1.uniSystemModule.platform;
            //如果为小程序的话不传值，默认为小程序类型
            userInfo.provider = provider || index_1.uniSystemModule.mpPlatform;
            userInfo.code = res[0] || '';
            var userInfoRes = res[1];
            userInfo.iv = userInfoRes.iv || '';
            userInfo.encryptedData = userInfoRes.encryptedData || '';
            var userInfoRO = userInfoRes.userInfo;
            userInfo.openId = userInfoRO.openId || '';
            userInfo.unionId = userInfoRO.unionId || '';
            userInfo.nickName = userInfoRO.nickName || '';
            userInfo.avatarUrl = userInfoRO.avatarUrl || '';
            userInfo.gender = userInfoRO.gender || 0;
            userInfo.country = userInfoRO.country || '';
            userInfo.province = userInfoRO.province || '';
            userInfo.city = userInfoRO.city || '';
            if (index_1.uniSystemModule.isApp) {
                if (provider === UniProviderType_1.default.qq) {
                    userInfo.gender = userInfoRO.gender_type || 0;
                }
                //如果qq可以获取用户年龄
                userInfo.birthday = userInfoRO.year || '';
            }
            return userInfo;
        });
    };
    UniUserUtil.getUserInfo = function (provider) {
        return new Promise(function (resolve, reject) {
            if (index_1.uniSystemModule.isWX) {
                //只有为小程序，且为微信小程序时才
                uni.getUserProfile({
                    provider: provider,
                    desc: 'getUserInfo',
                    success: function (userInfo) {
                        resolve(userInfo);
                    },
                    fail: function (e) {
                        reject(e);
                    }
                });
            }
            else {
                uni.getUserInfo({
                    provider: provider,
                    success: function (userInfo) {
                        resolve(userInfo);
                    },
                    fail: function (e) {
                        reject(e);
                    }
                });
            }
        });
    };
    return UniUserUtil;
}());
exports.default = UniUserUtil;
//# sourceMappingURL=UniUserUtil.js.map