/// <reference types="@dcloudio/types" />
import LoginRes = UniApp.LoginRes;
export default class UniLoginUtil {
    static getLoginCode(provider?: any): Promise<string>;
    static login(provider?: any): Promise<LoginRes>;
}
