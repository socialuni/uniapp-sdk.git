/// <reference types="@dcloudio/types" />
import UniProviderLoginQO from "../model/UniProviderLoginQO";
import GetUserInfoRes = UniApp.GetUserInfoRes;
export default class UniUserUtil {
    static getUniProviderLoginQO(provider?: any): Promise<UniProviderLoginQO>;
    static getUserInfo(provider?: any): Promise<GetUserInfoRes>;
}
