"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var JsonUtil = /** @class */ (function () {
    function JsonUtil() {
    }
    // 改成any类型
    JsonUtil.deepClone = function (object) {
        return JSON.parse(JSON.stringify(object));
    };
    JsonUtil.toJson = function (object) {
        return JSON.stringify(object);
    };
    JsonUtil.jsonParse = function (objJson) {
        return JSON.parse(objJson);
    };
    JsonUtil.log = function (object) {
        console.log(JSON.stringify(object));
    };
    return JsonUtil;
}());
exports.default = JsonUtil;
//# sourceMappingURL=JsonUtil.js.map