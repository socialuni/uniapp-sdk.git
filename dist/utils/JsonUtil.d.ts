export default class JsonUtil {
    static deepClone(object: any): any;
    static toJson(object: any): string;
    static jsonParse(objJson: string): any;
    static log(object: any): void;
}
