本项目非ui组件，而是js功能插件，sdk

演示项目界面比较丑陋，仅展示jdk支持的功能，请见谅

项目提取自社交联盟项目，基于业务提取，可能覆盖渠道不是很广，欢迎大家提pr，
项目稳定，会长期维护

后台项目里提供了一个用来演示的数据库地址，可以直接使用

关联登录模板 [uniapp-sdk-login-demo](https://ext.dcloud.net.cn/plugin?id=5438)

码云地址 

[插件地址](https://gitee.com/socialuni/uniapp-sdk)
[插件后端项目地址](https://gitee.com/socialuni/uniapp-sdk-java)

[前端演示项目](https://gitee.com/socialuni/common-user)
[后端演示项目](https://gitee.com/socialuni/common-user-demo)
[后端用户模块](https://gitee.com/socialuni/common-user-sdk)
[后端用户数据库对象](https://gitee.com/socialuni/common-user-model)
[后端通用工具类](https://gitee.com/socialuni/common-utils)

uniapp，qq、微信等渠道相关通用逻辑提取成sdk插件，基于社交联盟业务提取，已上线使用


0.0.1
开源代码，仅够交流，uniapp，qq、微信等渠道相关通用逻辑提取成sdk插件，基于社交联盟业务提取，已上线使用。
使用方式

注意事项，需要在vue.config.js中配置
```
module.exports = {
  transpileDependencies: ['uniapp-sdk']
}
```

安装使用方法

```
npm install uniapp-sdk
```

main.js
```
Vue.use(unisdk, {store})
```
逻辑
```
const loginQO = await UniUserUtil.getUniProviderLoginQO()
```
```
//loginQO json
{
  "provider": "weixin",
  "platform": "mp",
  "code": "021zz8000lxfWL1dML000p3KBq1zz80D",
  "openId": "",
  "unionId": "",
  "encryptedData": "uJ0Pa7++jAaa6k1sXfpXhWGrzF0XUdmCrED0HoZF5+0bRhffQm2m2U8bMvaNzd4eKPml64UMf+Xtap0+RPCc24I90UiSihYm75rg+4T/mgfT5kntUKDleCjkN",
  "iv": "9EM85/TZCFDAkjgcA2wecw==",
  "nickName": "活在梦里",
  "avatarUrl": "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqq1wLZcODakgia2nuLMgE0iaQfkX6GAtvvDA4KdtZHjkGvFoubucqKJusEaEMGLXyicH8CnSUsiapXQQ/132",
  "gender": 1,
  "birthday": "",
  "country": "China",
  "province": "Beijing",
  "city": "Chaoyang"
}
```
一行代码就可以获取登录所需要的信息

还可以配合后台使用，一键登录，记住用户

```
const {data} = await LoginAPI.providerLoginAPI(loginQO)
```
返回后台注册后入库的用户信息
```
//data
{
  "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxXzYyYjNmMzliMDMxMzQxMmVhNzIzNjk5ZDg1YjQ4YzMwIn0.6Dg4kpt5tM8xS7OJM8P1meWhjzwCHmLd6QKX0S_gG_E",
  "user": {
    "id": 1,
    "nickname": "活在梦里",
    "avatar": "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqq1wLZcODakgia2nuLMgE0iaQfkX6GAtvvDA4KdtZHjkGvFoubucqKJusEaEMGLXyicH8CnSUsiapXQQ/132",
    "gender": 1,
    "birthday": null,
    "age": 0,
    "city": "Chaoyang",
    "phoneNum": null
  }
}
```

后台项目在 [application-dev.yml](https://gitee.com/socialuni/common-user-demo/blob/master/src/main/resources/application-dev.yml) 中配置：，登录调用微信开放平台需要使用
```
uniapp:
  wx-mp-id: 
  wx-mp-secret: 
  qq-mp-id: 
  qq-mp-secret: 
```
